const makeFavorite = (data) => {
    // let allGoodsCollection = [...goodsCollection];
    // let favCollection = [...favorite];

    let allGoodsCollection = data;
    let favCollection =  JSON.parse(localStorage.getItem('favorite'));
    if(favCollection) {
      allGoodsCollection.forEach(item => {
        favCollection.forEach(fav => {
          if(item.article === fav.article) {
            item.isInFavorite = true;
          }
        })
      })
    }
    return allGoodsCollection
    // dispatch(actionGoodsCollection(allGoodsCollection))
    // setGoodsCollection(allGoodsCollection);
  }

export default makeFavorite