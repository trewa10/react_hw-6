import sendRequest from "./sendRequest";
import makeFavorite from "./makeFavorite";
export {sendRequest, makeFavorite};
