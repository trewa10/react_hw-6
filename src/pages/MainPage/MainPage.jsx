import React from "react";
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import ItemsList from "../../components/ItemsList";
import PageWrapper from "../../components/PageWrapper/PageWrapper";
import {selectGoodsCollection} from '../../store/selectors'

const MainPage = (props) => {
    const goodsCollection = useSelector(selectGoodsCollection);

    const {textBtn, widthBtn} = props;

    return (
        <PageWrapper>
            <h2>Look at our goods</h2>
            <ItemsList itemsList={goodsCollection} textBtn={textBtn} widthBtn={widthBtn}/>
        </PageWrapper>
    )
}

MainPage.propTypes = {
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string
}

export default MainPage;