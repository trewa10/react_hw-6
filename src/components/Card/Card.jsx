import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import {ReactComponent as FavoriteIcon} from './images/favorite.svg'
import {CardElement, CardFooter, CardMain, CardSection, StyledSpan, ImgContainer, ItemIMG} from './styledCard'
import {CardLi, LiFooter, LiMain, LiSection, ImgWrap, Description} from './styledCardTable'
import { useDispatch, useSelector } from 'react-redux';
import { selectCart, selectGoodsCollection } from '../../store/selectors';
import { actionCart, actionFavorite, actionGoodsCollection, actionTryToCart, actionTryToRemoveCart, actionModalText, actionModalTitle, actionNeedRemoveItem, actionIsModalOpen } from '../../store/actions'
import { SelectContext } from '../../context';

const Card = ({name, price, imgUrl, article, color, isRemove, isInFavorite, textBtn, widthBtn}) => {
    const dispatch = useDispatch();
    const cart = useSelector(selectCart);
    const goodsCollection = useSelector(selectGoodsCollection);
    const {tableView} = useContext(SelectContext);

    
    const addToFavorite = (article) => {
        let newGoodsCollection = goodsCollection.map(item => item.article === article ? {...item, isInFavorite: !item.isInFavorite} : item);
        let newCart = cart.map(item => item.article === article ? {...item, isInFavorite: !item.isInFavorite} : item);
        let newFavorite = newGoodsCollection.filter(item => item.isInFavorite);
        dispatch(actionFavorite(newFavorite))
        localStorage.setItem('favorite', JSON.stringify(newFavorite));
        dispatch(actionGoodsCollection(newGoodsCollection));
        dispatch(actionCart(newCart));
        localStorage.setItem('cart', JSON.stringify(newCart));
    }

    const addToCart = (article) => {
        let item = goodsCollection.filter(item => item.article === article)[0];
        dispatch(actionTryToCart(item));
        dispatch(actionModalText(item.name));
        dispatch(actionModalTitle('Do you want to add to cart?'));
        dispatch(actionNeedRemoveItem(false));
        dispatch(actionIsModalOpen(true));
    }

    const removeFromCart = (article) => {
        let item = cart.filter(item => item.article === article)[0];
        dispatch(actionTryToRemoveCart(item));
        dispatch(actionModalText(item.name));
        dispatch(actionModalTitle('Do you want to remove from cart?'));
        dispatch(actionNeedRemoveItem(true));
        dispatch(actionIsModalOpen(true));
    }

    if(tableView) {
        return (
        <CardLi>
            <ImgWrap>
                <img src={imgUrl} alt={name} />
            </ImgWrap>
            <LiSection>
                <h3>{name}</h3>
                <StyledSpan fontSize='18px'>Color: {color}</StyledSpan>
                <StyledSpan fontSize='14px'>Article: {article}</StyledSpan>
            </LiSection>
            <LiMain>
                <Description fontSize='24px'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores quo, praesentium porro possimus sequi quia eius voluptatibus ratione sunt error ipsum quam fugit architecto facilis doloribus facere placeat saepe. Debitis.</Description>
            </LiMain>
            <LiFooter>
                <Button width='auto' height='auto' onClick={() => addToFavorite(article)}>
                    <div>
                        <FavoriteIcon fill={isInFavorite ? 'orange' : 'white'} stroke={isInFavorite ? 'orange' : 'grey'}/>
                    </div>
                </Button>
                <Button backgroundColor='grey' width={widthBtn} onClick={isRemove ? () => removeFromCart(article) : () => addToCart(article)}>{textBtn}</Button>
                <StyledSpan fontSize='22px'>{price}$</StyledSpan>
            </LiFooter>
        </CardLi>
        )
    }

    return (
        <CardElement>
            <CardSection>
                <span>{name}</span>
                <Button width='auto' height='auto' onClick={() => addToFavorite(article)}>
                    <div>
                        <FavoriteIcon fill={isInFavorite ? 'orange' : 'white'} stroke={isInFavorite ? 'orange' : 'grey'}/>
                    </div>
                </Button>
            </CardSection>
            <CardMain>
                <StyledSpan fontSize='12px'>Article: {article}</StyledSpan>
                <ImgContainer>
                    <ItemIMG src={imgUrl} alt={name} />
                </ImgContainer>
                <StyledSpan>Color: {color}</StyledSpan>
            </CardMain>
            <CardFooter>
                <div>{price}$</div>
                <Button backgroundColor='grey' width={widthBtn} onClick={isRemove ? () => removeFromCart(article) : () => addToCart(article)}>{textBtn}</Button>
            </CardFooter>
        </CardElement>
    )
}


Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    imgUrl: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
    isRemove: PropTypes.bool,
    isInFavorite: PropTypes.bool,
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string
}

export default Card