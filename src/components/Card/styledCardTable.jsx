import styled from 'styled-components';

export const CardLi = styled.li`
    box-sizing: border-box;
    width: 100%;
    height: 186px;
    border: 1px solid grey;
    border-radius: 8px;
    margin-bottom: 4px;
    padding: 10px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`
export const LiSection = styled.div`
    width: 600px;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-between;
`
export const LiMain = styled(LiSection)`
    width: 100%;
    flex-direction: row;
    align-items: center;
` 
export const LiFooter = styled(LiSection)`
    width: min-content;
    margin-left: 10px;
    align-items: center;
` 
export const StyledSpan = styled.span`
    font-size: ${props => props.fontSize};
` 
export const Description = styled.p`
    font-size: ${props => props.fontSize};
    text-indent: 1.5em;
` 
export const ImgWrap = styled.div`
    margin: 5px 15px;
    
    img {
        width: auto;
        height: 150px;
    }
`
