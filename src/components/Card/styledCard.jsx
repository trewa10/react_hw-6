import styled from 'styled-components';

export const CardElement = styled.div`
    box-sizing: border-box;
    width: 300px;
    height: 370px;
    border: 1px solid grey;
    border-radius: 10px;
    padding: 10px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`
export const CardSection = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
`
export const CardMain = styled(CardSection)`
    flex-direction: column;
    align-items: flex-start;
` 
export const CardFooter = styled(CardSection)`
    /* position: absolute;
    bottom: 5px; */
` 
export const StyledSpan = styled.span`
    font-size: ${props => props.fontSize};
` 
export const ImgContainer = styled.div`
    margin: 5px 15px;
    align-self: flex-end;
    min-height: 200px;
    display: flex;
    align-items: center;
`

export const ItemIMG = styled.img`
    width: 150px;
    height: auto;
`