import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { SelectContext } from '../../context';
import Card from '../Card/Card';
import Button from '../Button';
import {ReactComponent as CardView} from './images/cardsView.svg'
import {ReactComponent as TableView} from './images/tableView.svg'

const ItemWrapper = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    gap: 30px;
    padding-bottom: 45px; 
`
const ItemTable = styled.ul`
    width: 100%;
    padding: 0;
`
const CardSwitcher = styled.div`
    width: 170px;
    height: 50px;
    position: absolute;
    top: -60px;
    right: 0;
    display: flex;
    gap: 5px;
    
`

const ItemsList =({itemsList, isRemove, textBtn, widthBtn}) => {
    const {tableView, showTable, showCard} = useContext(SelectContext);

    const cardsRender = itemsList.map(({name, price, imgUrl, article, color, isInFavorite}, index) => {
        return (
            (<Card key={(index)} isInFavorite={isInFavorite} name={name} price={price} imgUrl={imgUrl} 
            article={article} color={color} isRemove={isRemove} textBtn={textBtn} widthBtn={widthBtn}/>)
        )
    })

    return (
        <ItemWrapper>
            <CardSwitcher>
                <Button backgroundColor='gray' onClick={showCard}><CardView fill='white' width='20px'/>Plate</Button>
                <Button backgroundColor='gray' onClick={showTable}><TableView fill='white' width='20px'/>Table</Button>
            </CardSwitcher>
            {tableView || <>{cardsRender}</>}

            {tableView &&
                <ItemTable>
                    {cardsRender}
                </ItemTable>}
        </ItemWrapper>
        
    )
}

ItemsList.propTypes = {
    itemsList: PropTypes.array.isRequired,
    isRemove: PropTypes.bool,
    addToFavorite: PropTypes.func,
    widthBtn: PropTypes.string
}

export default ItemsList