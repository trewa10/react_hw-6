import styled from 'styled-components';

export const Dialog = styled.dialog`
    max-width: 800px;
    min-width: 550px;
    height: max-content;
    background: ${props => props.backgroundColor};
    border: 1px solid #888;
    border-radius: 8px;
    padding: 0; 
    position: relative;
    top:-50vh;

    ::backdrop {
        background: rgba(0, 0, 0, 0.5);
    }
`
export const ModalHeader = styled.div`
    display: flex;
    justify-content:space-between;
    background: rgba(0, 0, 0, 0.1);
    padding: 20px;
`
export const ModalTitle = styled.h2`
    color: #fff;
    font-weight: 700;
    margin: 0;
`
export const ModalText = styled.div`
    color: #fff;
    font-weight: 400;
    padding: 25px 20px;
    display: flex;
    justify-content: center;
    align-items: center;
`
export const ModalFooter = styled.div`
    display: flex;
    justify-content:center;
    padding: 10px;
    gap: 10px;
`
export const ModalCross = styled.div`
    width: 8px;
    height: 8px;
    position: relative;
    top: 15px;
    right: 10px;
    cursor: pointer;
    ::before, ::after {
        content: '';
        width: 20px;
        height: 2px;
        background: #fff;
        position: absolute;
    }
    ::before {transform: rotate(45deg);}
    ::after {transform: rotate(-45deg);}
`