import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {Dialog, ModalHeader, ModalTitle, ModalText, ModalFooter, ModalCross} from './styledModal';

const Modal = ({header, text, actions, closeButton, backgroundColor, closeClick}) => {
    const preventAutoClose = (e) => e.stopPropagation();

    const escFunction = (e) => {
        if(e.keyCode === 27) {
            closeClick();
        }
      }

      useEffect(() => {
        document.addEventListener("keydown", escFunction);

        return function clean() {
            document.removeEventListener("keydown", escFunction);
        }
      })
      
      return (
          <Dialog backgroundColor={backgroundColor} onClick={closeClick}>
              <div onClick={preventAutoClose}>
                  <ModalHeader>
                      <ModalTitle>{header}</ModalTitle>
                      {closeButton && <ModalCross onClick={closeClick}/>}
                  </ModalHeader>
                  <ModalText>{text}</ModalText>
                  <ModalFooter>
                      {actions}
                  </ModalFooter>
              </div>
          </Dialog>
      )
    
}


Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.element,
    closeButton: PropTypes.bool,
    backgroundColor: PropTypes.string,
    closeClick: PropTypes.func
}

Modal.defaultProps = {
    closeButton: true,
    backgroundColor: 'grey'
}

export default Modal;