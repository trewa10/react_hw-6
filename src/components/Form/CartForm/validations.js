import * as yup from 'yup';

export const validationSchema = yup.object({
	name: yup
		.string('Enter your name')
		.required('Name is required')
		.min(2, "Name is too short"),
	surname: yup
		.string('Enter your profession')
		.required('Surname is required')
		.min(3, "Surname is too short"),
	age: yup
		.number('Enter your age')
		.lessThan(101, 'Are you serious?')
		.moreThan(15, 'Sorry, but you have to be older')
		.required('Age is required'),
	phone: yup
		.string('Enter your phone number')
		.required('Phone number is required')
		.matches(/.(\d{2})\s\((\d{3})\)\s(\d{3})-(\d{2})-(\d{2})/gm, {message: 'Phone number is required'}),
	adress: yup
		.string('Enter your adress')
		.required('Delivery address is required')
		.matches(/(\d{1,})\s[a-zA-Z0-9\s]+(\.)?\s[a-zA-Z]+(,)?\s[0-9]{5,6}/igm, 
				{message: 'Please, enter correct adress. Example: 42 Vasylkivska street Kyiv, 01044'}),
});
