import {createAction} from "@reduxjs/toolkit";
import {sendRequest, makeFavorite} from "../helpers";

export const actionIsModalOpen = createAction('ACTION_IS_MODAL_OPEN')
export const actionGoodsCollection = createAction('ACTION_GOODS_COLLECTION')
export const actionCart = createAction('ACTION_CART')
export const actionFavorite = createAction('ACTION_FAVORITE')
export const actionTryToCart = createAction('ACTION_TRY_TO_CART')
export const actionTryToRemoveCart = createAction('ACTION_TRY_TO_REMOVE_CART')
export const actionModalText = createAction('ACTION_MODAL_TEXT')
export const actionModalTitle = createAction('ACTION_MODAL_TITLE')
export const actionNeedRemoveItem = createAction('ACTION_NEED_REMOVE_ITEM')

export const actionFetchGoodsCollection = () => (dispatch) => {
    // dispatch(actionLoader(true))
    return sendRequest(`${document.location.origin}/externalGoods.JSON`)
        .then(data => makeFavorite(data))
        .then((data) => {
            dispatch(actionGoodsCollection(data))
            // dispatch(actionLoader(false))
        })
}



