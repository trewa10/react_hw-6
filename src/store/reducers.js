import {createReducer} from "@reduxjs/toolkit";
import * as actions from './actions'

const initialState = {
    isModalOpen: false,
    goodsCollection: [],
    cart: [],
    favorite: [],
    tryToCart: {},
    tryToRemoveCart: {},
    modalText: '',
    modalTitle: '',
    needRemoveItem: false,
}

export default createReducer(initialState, (builder) => {
    builder
    .addCase(actions.actionIsModalOpen, (state, {payload}) => {
        state.isModalOpen = payload;
    })
    .addCase(actions.actionGoodsCollection, (state, {payload}) => {
        state.goodsCollection = payload;
    })
    .addCase(actions.actionCart, (state, {payload}) => {
        state.cart = payload;
    })
    .addCase(actions.actionFavorite, (state, {payload}) => {
        state.favorite = payload;
    })
    .addCase(actions.actionTryToCart, (state, {payload}) => {
        state.tryToCart = payload;
    })
    .addCase(actions.actionTryToRemoveCart, (state, {payload}) => {
        state.tryToRemoveCart = payload;
    })
    .addCase(actions.actionModalText, (state, {payload}) => {
        state.modalText = payload;
    })
    .addCase(actions.actionModalTitle, (state, {payload}) => {
        state.modalTitle = payload;
    })
    .addCase(actions.actionNeedRemoveItem, (state, {payload}) => {
        state.needRemoveItem = payload;
    })
})